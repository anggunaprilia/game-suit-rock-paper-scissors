const DRAW = 'draw'
const P_WIN = 'player win'
const C_WIN = 'computer win'

let initScorePlayer = 0
let initScoreComputer = 0

let scorePlayer = document.getElementById('score-player')
let scoreComputer = document.getElementById('score-computer')

let imgPlayer = document.getElementById('img-player')
let imgComputer = document.getElementById('img-computer')

let playBtn = document.getElementsByClassName('play-btn')

let imgPath = 'gambar'
let options = ['rock', 'paper', 'scissor']
function play(option) {
    let playerDesicion = setImage(option)
    let comDesicion = setImageComputer()
    setTheWinner(playerDesicion, comDesicion)
}

function setImage(option){
    imgPlayer.src = imgPath+'/'+ option +'-player.png'
    return option
}

function setImageComputer() {
    let computerDecision = options[Math.floor(Math.random()*options.length)]
    imgComputer.src= imgPath+'/'+ computerDecision +'-bot.png'
    return computerDecision
}

function setTheWinner(playerDesicion, comDesicion) {
    let result = ''
    if(playerDesicion == 'rock'){
        switch(comDesicion){
            case 'rock':
                result = DRAW
            break;
            case 'paper':
                result = C_WIN
            break;
            case 'sciccor':
                result = P_WIN
            break;
        }
    }
    if(playerDesicion == 'paper'){
        switch(comDesicion){
            case 'rock':
                result = P_WIN
            break;
            case 'paper':
                result = DRAW
            break;
            case 'sciccor':
                result = C_WIN
            break;
        }
    }
    if(playerDesicion == 'scissor'){
        switch(comDesicion){
            case 'rock':
                result = C_WIN
            break;
            case 'paper':
                result = P_WIN
            break;
            case 'sciccor':
                result = DRAW
            break;
        }
    }

    scoring(result)
}

function scoring(result){
    if(result == 'player win'){
        initScorePlayer++
        scorePlayer.innerHTML = initScorePlayer

        if(initScorePlayer >= 3) {
            for(let i=0; i < playBtn.length; i++){
                playBtn[i].setAttribute('disabled', '')
            }

            playAgain(P_WIN)
        }
    }
    if (result == 'computer win'){
        initScoreComputer++
        scoreComputer.innerHTML = initScoreComputer

        if(initScoreComputer >= 3) {
            for(let i=0; i < playBtn.length; i++){
                playBtn[i].setAttribute('disabled', '')
            }

            playAgain(C_WIN)
        }
    }


}

function playAgain(winner){
    if(confirm(winner+'. play again?')){

        initScoreComputer = 0
        initScorePlayer = 0

        scorePlayer.innerHTML = initScorePlayer
        scoreComputer.innerHTML = initScoreComputer

        for(let i=0; i < playBtn.length; i++){
            playBtn[i].removeAttribute('disabled')
        }
    }
}

